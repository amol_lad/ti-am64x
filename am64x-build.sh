#!/bin/bash

# This script takes one optional argument - the path where all downloaded
# packages should be kept. If not provided, it uses the "dl" folder in the
# current directory

# exit on any error
set -e

BASE_DIR=$(pwd)
DL_DIR="$1"
[[ -z "$DL_DIR" ]] && {
	DL_DIR="$(pwd)/dl"
	mkdir -p "$DL_DIR"
}

BUILD_DIR="$BASE_DIR/build"
mkdir -p "$BUILD_DIR"

BIN_DIR="$BUILD_DIR/bin"
mkdir -p "$BIN_DIR"

TMP_DIR="$BUILD_DIR/tmp"
mkdir -p "$TMP_DIR"

EXTRA_DRIVERS="$BUILD_DIR/extra-drivers"
mkdir -p "$EXTRA_DRIVERS"

LINUX_FIRMWARE_DIR="$BIN_DIR/firmware"
mkdir -p "$LINUX_FIRMWARE_DIR"

# Utility function to download the package and
# apply patches if any
function pkg_download() {
	local pkg_name pkg_url pkg_dir pkg_file extension tmpdir

	pkg_name="$1"
	pkg_file="$2"
	pkg_dir="$3"
	pkg_url="$4"
	parent_pkg_dir=$(dirname "$pkg_dir")

	if [[ ! -d "$pkg_dir" ]]; then
		mkdir -p "$parent_pkg_dir"
		cd "$parent_pkg_dir"
		[[ ! -f "$DL_DIR/$pkg_file" ]] && {
			tmpdir=$(mktemp -d)
			if wget "$pkg_url" -P "$tmpdir"; then
				mv -v "$tmpdir/$pkg_file" "$DL_DIR/"
			fi
			rm -rf "$tmpdir"
		}
		echo "Extracting $pkg_file .."
		extension="${pkg_file##*.}"
		case "$extension" in
			xz|txz) tar -xf "$DL_DIR/$pkg_file" ;;
			gz|tgz) tar -zxf "$DL_DIR/$pkg_file" ;;
		esac
		# patch
		cd "$pkg_dir"
		[[ -d "$BASE_DIR"/src/"$pkg_name" ]] && cp -a "$BASE_DIR"/src/"$pkg_name"/* .
		[[ -d "$BASE_DIR"/patches/"$pkg_name" ]] && {
			for f in "$BASE_DIR"/patches/"$pkg_name"/*.patch; do
				patch -p1 -i "$f"
			done
		}
	fi

	return 0
}

###################################################################################################################################
#							DOWNLOAD Toolchain

# ARM toolchain version
TOOLCHAIN_VERSION="10.3-2021.07"

PKG_NAME="gcc-arm-$TOOLCHAIN_VERSION-x86_64-aarch64-none-linux-gnu"
PKG_FILE="$PKG_NAME.tar.xz"
PKG_SOURCE_URL="https://developer.arm.com/-/media/Files/downloads/gnu-a/$TOOLCHAIN_VERSION/binrel/$PKG_FILE"
PKG_BUILD_DIR="$BUILD_DIR/toolchain/$PKG_NAME"

pkg_download "$PKG_NAME" "$PKG_FILE" "$PKG_BUILD_DIR" "$PKG_SOURCE_URL"
export PATH="$PKG_BUILD_DIR"/bin/:"$PATH"

PKG_NAME="gcc-arm-$TOOLCHAIN_VERSION-x86_64-arm-none-linux-gnueabihf"
PKG_FILE="$PKG_NAME.tar.xz"
PKG_SOURCE_URL="https://developer.arm.com/-/media/Files/downloads/gnu-a/$TOOLCHAIN_VERSION/binrel/$PKG_FILE"
PKG_BUILD_DIR="$BUILD_DIR/toolchain/$PKG_NAME"

pkg_download "$PKG_NAME" "$PKG_FILE" "$PKG_BUILD_DIR" "$PKG_SOURCE_URL"
export PATH="$PKG_BUILD_DIR"/bin/:"$PATH"


###################################################################################################################################


###################################################################################################################################
#							ATF
ATF_TAG=08.02.00.002

PKG_NAME="arm-trusted-firmware"
PKG_VERSION="$ATF_TAG"
PKG_FILE="$PKG_NAME-$PKG_VERSION.tar.xz"
PKG_SOURCE_URL="https://git.ti.com/cgit/atf/$PKG_NAME/snapshot/$PKG_FILE"
PKG_BUILD_DIR="$BUILD_DIR/$PKG_NAME-$PKG_VERSION"

pkg_download "$PKG_NAME" "$PKG_FILE" "$PKG_BUILD_DIR" "$PKG_SOURCE_URL"

cd "$PKG_BUILD_DIR"
echo "Building $PKG_NAME.."
make -j32 CROSS_COMPILE=aarch64-none-linux-gnu- ARCH=aarch64 PLAT=k3 TARGET_BOARD=lite SPD=opteed
cp build/k3/lite/release/bl31.bin "$TMP_DIR"

###################################################################################################################################


###################################################################################################################################
#							OPTEE
OPTEE_OS_TAG=3.16.0

PKG_NAME="optee_os"
PKG_VERSION="$OPTEE_OS_TAG"
PKG_FILE="$PKG_VERSION.tar.gz"
PKG_SOURCE_URL="https://github.com/OP-TEE/optee_os/archive/refs/tags/$PKG_FILE"
PKG_BUILD_DIR="$BUILD_DIR/$PKG_NAME-$PKG_VERSION"

pkg_download "$PKG_NAME" "$PKG_FILE" "$PKG_BUILD_DIR" "$PKG_SOURCE_URL"

cd "$PKG_BUILD_DIR"
echo "Building $PKG_NAME.."
make -j32 ARCH=arm PLATFORM=k3-am64x CROSS_COMPILE32=arm-none-linux-gnueabihf- CROSS_COMPILE64=aarch64-none-linux-gnu- CFG_ARM64_core=y
cp out/arm-plat-k3/core/tee-pager_v2.bin "$TMP_DIR"

###################################################################################################################################

###################################################################################################################################
#							Linux Firmware (PRUSS)
LINUX_FIRMWARE_TAG=08.02.00.002

PKG_NAME="ti-linux-firmware"
PKG_VERSION="$LINUX_FIRMWARE_TAG"
PKG_FILE="$PKG_NAME-$PKG_VERSION.tar.gz"
PKG_SOURCE_URL="https://git.ti.com/cgit/processor-firmware/$PKG_NAME/snapshot/$PKG_FILE"
PKG_BUILD_DIR="$BUILD_DIR/$PKG_NAME-$PKG_VERSION"

pkg_download "$PKG_NAME" "$PKG_FILE" "$PKG_BUILD_DIR" "$PKG_SOURCE_URL"

echo "Copying Linux Firmware Images"
rm -f "$LINUX_FIRMWARE_DIR"/*
cp -v "$PKG_BUILD_DIR"/ti-pruss/am65x-sr2-pru0-prueth-fw.elf "$LINUX_FIRMWARE_DIR"
cp -v "$PKG_BUILD_DIR"/ti-pruss/am65x-sr2-pru1-prueth-fw.elf "$LINUX_FIRMWARE_DIR"
cp -v "$PKG_BUILD_DIR"/ti-pruss/am65x-sr2-txpru0-prueth-fw.elf "$LINUX_FIRMWARE_DIR"
cp -v "$PKG_BUILD_DIR"/ti-pruss/am65x-sr2-txpru1-prueth-fw.elf "$LINUX_FIRMWARE_DIR"
cp -v "$PKG_BUILD_DIR"/ti-pruss/am65x-sr2-rtu0-prueth-fw.elf "$LINUX_FIRMWARE_DIR"
cp -v "$PKG_BUILD_DIR"/ti-pruss/am65x-sr2-rtu1-prueth-fw.elf "$LINUX_FIRMWARE_DIR"

###################################################################################################################################

###################################################################################################################################
#							U-boot
U_BOOT_TAG=08.02.00.002

PKG_NAME="ti-u-boot"
PKG_VERSION="$U_BOOT_TAG"
PKG_FILE="$PKG_NAME-$PKG_VERSION.tar.xz"
PKG_SOURCE_URL="https://git.ti.com/cgit/$PKG_NAME/$PKG_NAME/snapshot/$PKG_FILE"
PKG_BUILD_DIR="$BUILD_DIR/$PKG_NAME-$PKG_VERSION"

pkg_download "$PKG_NAME" "$PKG_FILE" "$PKG_BUILD_DIR" "$PKG_SOURCE_URL"

### Build U-boot ###

U_BOOT_OUTDIR="$TMP_DIR"/u-boot-out
mkdir -p "$U_BOOT_OUTDIR"/r5
mkdir -p "$U_BOOT_OUTDIR"/a53

U_BOOT_R5_DEFCONFIG=am64x_aprisalte_r5_defconfig
U_BOOT_A53_DEFCONFIG=am64x_aprisalte_a53_defconfig
cd "$PKG_BUILD_DIR"

# 	R5

make -j32 ARCH=arm CROSS_COMPILE=arm-none-linux-gnueabihf- "$U_BOOT_R5_DEFCONFIG" O="$U_BOOT_OUTDIR"/r5
make -j32 ARCH=arm CROSS_COMPILE=arm-none-linux-gnueabihf- O="$U_BOOT_OUTDIR"/r5

# 	A53

make -j32 ARCH=arm CROSS_COMPILE=aarch64-none-linux-gnu- "$U_BOOT_A53_DEFCONFIG" O="$U_BOOT_OUTDIR"/a53
make -j32 ARCH=arm CROSS_COMPILE=aarch64-none-linux-gnu- ATF="$TMP_DIR"/bl31.bin TEE="$TMP_DIR"/tee-pager_v2.bin O="$U_BOOT_OUTDIR"/a53

# copy images

cp "$U_BOOT_OUTDIR"/a53/tispl.bin "$BIN_DIR"
cp "$U_BOOT_OUTDIR"/a53/u-boot.img "$BIN_DIR"

###################################################################################################################################


###################################################################################################################################
#							K3 Image Gen (tiboot3.bin)
IMAGE_GEN_TAG=08.02.00.002

PKG_NAME="k3-image-gen"
PKG_VERSION="$IMAGE_GEN_TAG"
PKG_FILE="$PKG_NAME-$PKG_VERSION.tar.xz"
PKG_SOURCE_URL="https://git.ti.com/cgit/$PKG_NAME/$PKG_NAME/snapshot/$PKG_FILE"
PKG_BUILD_DIR="$BUILD_DIR/$PKG_NAME-$PKG_VERSION"

pkg_download "$PKG_NAME" "$PKG_FILE" "$PKG_BUILD_DIR" "$PKG_SOURCE_URL"

# tiboot3.bin (sysfw.itb is combined with tiboot3.bin)
# This must be built after u-boot build as it uses r5 u-boot-spl

cd "$PKG_BUILD_DIR"
make CROSS_COMPILE=arm-none-linux-gnueabihf- SOC=am64x SBL="$U_BOOT_OUTDIR"/r5/spl/u-boot-spl.bin

cp "$PKG_BUILD_DIR"/tiboot3.bin "$BIN_DIR"

###################################################################################################################################


###################################################################################################################################
#							Linux Kernel
KERNEL_TAG=08.02.00.002

PKG_NAME="ti-linux-kernel"
PKG_VERSION="$KERNEL_TAG"
PKG_FILE="$PKG_NAME-$PKG_VERSION.tar.xz"
PKG_SOURCE_URL="https://git.ti.com/cgit/$PKG_NAME/$PKG_NAME/snapshot/$PKG_FILE"
PKG_BUILD_DIR="$BUILD_DIR/$PKG_NAME-$PKG_VERSION"
LINUX_DIR="$BUILD_DIR/$PKG_NAME-$PKG_VERSION"

pkg_download "$PKG_NAME" "$PKG_FILE" "$PKG_BUILD_DIR" "$PKG_SOURCE_URL"

### Build Linux ###

#LINUX_DEFCONFIG=tisdk_am64xx-evm_defconfig
LINUX_DEFCONFIG=aprisalte-am64xx_defconfig
export CROSS_COMPILE=aarch64-none-linux-gnu-
export ARCH=arm64

cd "$PKG_BUILD_DIR"
make -j32 "$LINUX_DEFCONFIG"
make -j32 Image dtbs
make -j32 modules
KERNEL_VERSION=$(make kernelversion)

MODULES_DIR="$BIN_DIR/modules"
MODULES_INSTALL_DIR="$MODULES_DIR/lib/modules/$KERNEL_VERSION/"

rm -rf "$MODULES_DIR"
make -j32 modules_install INSTALL_MOD_PATH="$MODULES_DIR"
# backup
rm -rf "$TMP_DIR"/modules
make -j32 modules_install INSTALL_MOD_PATH="$TMP_DIR"/modules

# uncompressed kernel image
cp arch/arm64/boot/Image "$BIN_DIR"
# LZ4 compressed kernel image
{ cat arch/arm64/boot/Image | lz4c -c1 stdin stdout; printf \010\020\350\000; } > "$BIN_DIR"/Image.lz4
# DTB
cp arch/arm64/boot/dts/ti/am64x-aprisalte.dtb "$BIN_DIR"/image-ti-am64x-aprisalte.dtb
# copy all modules to root modules folder as expected by openwrt's kmodloader
cd "$MODULES_INSTALL_DIR"
tmpdir=$(mktemp -d)
find . -iname "*.ko" -exec cp {} "$tmpdir"/ \;
rm -rf *
cp "$tmpdir"/* .
rm -rf "$tmpdir"

##### Build out of the tree kernel modules #########

### aprisamcu_kerneldriver ###

cd "$BUILD_DIR"

PKG_NAME="aprisamcu_kerneldriver"
PKG_VERSION="1.0"
PKG_FILE="$PKG_NAME-$PKG_VERSION.tar.xz"
[[ ! -f "$DL_DIR/$PKG_FILE" ]] && {
	echo "$DL_DIR/$PKG_FILE missing.. aborting"
	exit 1
}

[[ ! -d "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION" ]] && tar -C "$EXTRA_DRIVERS" -xf "$DL_DIR/$PKG_FILE"

make -C "$LINUX_DIR" M="$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION"

cp "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION"/aprisamcu.ko "$MODULES_INSTALL_DIR"

### Laird backport ###

cd "$BUILD_DIR"

PKG_NAME="laird-backport"
PKG_VERSION="9.32.0.6"
PKG_FILE="backports-laird-$PKG_VERSION.tar.bz2"
[[ ! -f "$DL_DIR/$PKG_FILE" ]] && {
	echo "$DL_DIR/$PKG_FILE missing.. aborting"
	exit 1
}

[[ ! -d "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION" ]] && tar -C "$EXTRA_DRIVERS" -xf "$DL_DIR/$PKG_FILE"

make -C "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION" KLIB_BUILD="$LINUX_DIR" defconfig-sterling60
make -C "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION" KLIB_BUILD="$LINUX_DIR"

cp "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION"/drivers/net/wireless/laird/lrdmwl/lrdmwl.ko "$MODULES_INSTALL_DIR"
cp "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION"/drivers/net/wireless/laird/lrdmwl/lrdmwl_usb.ko "$MODULES_INSTALL_DIR"
cp "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION"/net/mac80211/mac80211.ko "$MODULES_INSTALL_DIR"
cp "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION"/net/wireless/cfg80211.ko "$MODULES_INSTALL_DIR"
cp "$EXTRA_DRIVERS/$PKG_NAME-$PKG_VERSION"/compat/compat.ko "$MODULES_INSTALL_DIR"

###################################################################################################################################

