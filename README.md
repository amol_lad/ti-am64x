# AprisaLTE TI AM64x based  build scripts

## Introduction
Main intention of this repository is to build a TI AM64x kernel, u-boot and other necessary firmware such as ATF/OPTEE

## Build with host tools
host-install.sh: Install necessary host packages
am64x-build.sh: Download and build am64x images

## Booting using UART boot mode in Ubuntu

1] setup boot resistors to boot from UART [ or in case of TI AM64x EVM setup dip switch Bootmode 0-7 -> SW2.4 = ON, SW2.5 = ON, SW2.6 = ON, SW2.7 = OFF]
2] Power cycle radio
2] The console will be detected at /dev/ttyUSB0. Configure it: 115200 8N1. Note: Every time you connect/disconnect USB cable, you need to reconfigure the port
	# sudo stty -F /dev/ttyUSB0 115200 cs8 -parenb -ixon -cstopb
3] Transfer binary. Open Minicom as soon as third binary transfer is complete so you can break at u-boot prompt
	# sudo bash
	# sx -kb tiboot3.bin < /dev/ttyUSB0 > /dev/ttyUSB0
	# sx -kb --ymodem tispl.bin < /dev/ttyUSB0 > /dev/ttyUSB0
	# sx -kb --ymodem u-boot.img < /dev/ttyUSB0 > /dev/ttyUSB0
	# <open minicom as soon as above is done>
4] All done!

