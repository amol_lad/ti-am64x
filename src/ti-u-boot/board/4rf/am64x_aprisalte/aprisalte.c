// SPDX-License-Identifier: GPL-2.0+
/*
 * Board specific initialization for AM642 Aprisa LTE
 *
 * Copyright (C) 2022 4RF Limited - www.4rf.com
 *	Amol Lad <amol.lad@4rf.com>
 *
 */

#include <common.h>
#include <asm/io.h>
#include <env.h>
#include <net.h>
#include <spl.h>
#include <fdt_support.h>
#include <asm/gpio.h>
#include <asm/arch/hardware.h>
#include <asm/arch/sys_proto.h>

//#include "am64x_aprisalte.h"

DECLARE_GLOBAL_DATA_PTR;

int board_init(void)
{
	return 0;
}

int dram_init(void)
{
	gd->ram_size = 0x40000000;

	return 0;
}

int dram_init_banksize(void)
{
	/* Bank 0 declares the memory available in the DDR low region */
	gd->bd->bi_dram[0].start = CONFIG_SYS_SDRAM_BASE;
	gd->bd->bi_dram[0].size = 0x40000000;
	gd->ram_size = 0x40000000;

	return 0;
}
#ifdef CONFIG_SPL_LOAD_FIT
int board_fit_config_name_match(const char *name)
{
	return 0;
}
#endif

#ifdef CONFIG_BOARD_LATE_INIT
int board_late_init(void)
{
	return 0;
}
#endif

#define CTRLMMR_USB0_PHY_CTRL	0x43004008
#define CORE_VOLTAGE		0x80000000

#ifdef CONFIG_SPL_BOARD_INIT
void spl_board_init(void)
{
	u32 val;
	/* Set USB PHY core voltage to 0.85V */
	val = readl(CTRLMMR_USB0_PHY_CTRL);
	val &= ~(CORE_VOLTAGE);
	writel(val, CTRLMMR_USB0_PHY_CTRL);

	/* Init DRAM size for R5/A53 SPL */
	dram_init_banksize();
}
#endif
