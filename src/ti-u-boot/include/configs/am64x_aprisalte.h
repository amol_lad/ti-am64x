/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Configuration header file for K3 AM642 SoC family
 *
 * Copyright (C) 2020 Texas Instruments Incorporated - https://www.ti.com/
 *	Keerthy <j-keerthy@ti.com>
 */

#ifndef __CONFIG_AM642_APRISALTE_H
#define __CONFIG_AM642_APRISALTE_H

#include <linux/sizes.h>
#include <config_distro_bootcmd.h>
#include <environment/ti/mmc.h>
#include <asm/arch/am64_hardware.h>
#include <environment/ti/k3_dfu.h>

/* DDR Configuration */
#define CONFIG_SYS_SDRAM_BASE1		0x880000000

#ifdef CONFIG_SYS_K3_SPL_ATF
#define CONFIG_SPL_FS_LOAD_PAYLOAD_NAME	"tispl.bin"
#endif

#ifndef CONFIG_CPU_V7R
#define CONFIG_SKIP_LOWLEVEL_INIT
#endif

#define CONFIG_SPL_MAX_SIZE		CONFIG_SYS_K3_MAX_DOWNLODABLE_IMAGE_SIZE
#if defined(CONFIG_TARGET_AM642_A53_APRISALTE)
#define CONFIG_SYS_INIT_SP_ADDR         (CONFIG_SPL_TEXT_BASE +	\
					CONFIG_SYS_K3_NON_SECURE_MSRAM_SIZE - 4)
#else
/*
 * Maximum size in memory allocated to the SPL BSS. Keep it as tight as
 * possible (to allow the build to go through), as this directly affects
 * our memory footprint. The less we use for BSS the more we have available
 * for everything else.
 */
#define CONFIG_SPL_BSS_MAX_SIZE		0x4000
/*
 * Link BSS to be within SPL in a dedicated region located near the top of
 * the MCU SRAM, this way making it available also before relocation. Note
 * that we are not using the actual top of the MCU SRAM as there is a memory
 * location filled in by the boot ROM that we want to read out without any
 * interference from the C context.
 */
#define CONFIG_SPL_BSS_START_ADDR	(TI_SRAM_SCRATCH_BOARD_EEPROM_START -\
					 CONFIG_SPL_BSS_MAX_SIZE)
/* Set the stack right below the SPL BSS section */
#define CONFIG_SYS_INIT_SP_ADDR         CONFIG_SPL_BSS_START_ADDR
/* Configure R5 SPL post-relocation malloc pool in DDR */
#define CONFIG_SYS_SPL_MALLOC_START    0x84000000
#define CONFIG_SYS_SPL_MALLOC_SIZE     SZ_16M
#endif

#define CONFIG_SYS_BOOTM_LEN            SZ_64M

#define EXTRA_ENV_DFUARGS \
	DFU_ALT_INFO_MMC \
	DFU_ALT_INFO_EMMC \
	DFU_ALT_INFO_RAM

#define DEFAULT_APRISALTE_ENV \
	"fit_addr=0x90000000\0" \
	"verify=1\0" \
	"console=ttyS2,115200n8\0" \
	"autoload=no\0" \
	"bootargs=console=ttyS2,115200n8 usbcore.authorized_default=0\0" \
	"bootfailaction=exit\0" \
	"APRISALTE_BOOT_VER=4\0"

#define FACT_UPDATE_USB "fact_update_usb=" \
				"usb start; " \
				"setenv verify; " \
				"fatload usb 0:1 ${fit_addr} kernel-am64x.fit || exit; " \
				"imxtract ${fit_addr} kernel-1 ${kernel_addr_r} || exit; " \
				"imxtract ${fit_addr} fdt-1 ${fdt_addr_r} || exit; " \
				"fatload usb 0:1 ${fit_addr} factory-initramfs.fit || exit; " \
				"usb stop; " \
				"setenv bootargs ${bootargs} usbload; " \
				"booti ${kernel_addr_r} ${fit_addr} ${fdt_addr_r} || exit " "\0"

#define FACT_UPDATE_NET "fact_update_net=" \
				"dhcp; " \
				"setenv verify; " \
				"tftpboot ${fit_addr} ${serverip}:${serverpath}/kernel-am64x.fit || exit; " \
				"imxtract ${fit_addr} kernel-1 ${kernel_addr_r} || exit; " \
				"imxtract ${fit_addr} fdt-1 ${fdt_addr_r} || exit; " \
				"tftpboot ${fit_addr} ${serverip}:${serverpath}/factory-initramfs.fit || exit; " \
				"booti ${kernel_addr_r} ${fit_addr} ${fdt_addr_r} || exit" "\0"

#define FACT_UPDATE_TFTP "fact_update_tftp=setenv bootargs ${bootargs} tftpload=${serverip}:${serverpath}; run fact_update_net" "\0"

#define FACT_UPDATE_FTP "fact_update_ftp=setenv bootargs ${bootargs} ftpload=${serverip}:${serverpath}; run fact_update_net" "\0"

#define GET_ACTIVE_SOFTWARE "get_active_software=" \
				"mmc partconf 0 activepart; " \
				"if test \"${activepart}\" = \"1\"; then " \
					"setenv KERNPART 0:3; " \
					"setenv ROOT /dev/mmcblk0p4; " \
				"else " \
					"setenv KERNPART 0:5; " \
					"setenv ROOT /dev/mmcblk0p6; " \
				"fi" "\0"

#define LOAD_SW_IMAGES "load_sw_images=" \
				"ext2load mmc $KERNPART ${fit_addr} kernel-am64x.fit || $bootfailaction; " \
				"imxtract ${fit_addr} kernel-1 ${kernel_addr_r} || exit; " \
				"imxtract ${fit_addr} fdt-1 ${fdt_addr_r} || exit " "\0"

#define BOOT_APRISALTE "boot_aprisalte=" \
				"run get_active_software; " \
				"run load_sw_images; " \
				"source ${fit_addr}:script || $bootfailaction; " \
				"setenv bootargs $bootargs root=/dev/dm-0 bootver=${APRISALTE_BOOT_VER}; " \
				"setenv bootargs $bootargs dm-mod.create='\"'lroot,,,ro,0 ${verity_sectors} verity 1 ${ROOT} ${ROOT} ${verity_data_block_sz} ${verity_hash_block_sz} ${verity_data_blocks} ${verity_hash_start} ${verity_hash_alg} ${verity_root_hash} ${verity_salt}'\"'; " \
				"booti ${kernel_addr_r} - ${fdt_addr_r} || $bootfailaction" "\0"

/* Incorporate settings into the U-Boot environment */
#define CONFIG_EXTRA_ENV_SETTINGS					\
	DEFAULT_LINUX_BOOT_ENV						\
	DEFAULT_FIT_TI_ARGS						\
	DEFAULT_MMC_TI_ARGS						\
	EXTRA_ENV_DFUARGS						\
	DEFAULT_APRISALTE_ENV \
	FACT_UPDATE_USB \
	FACT_UPDATE_NET \
	FACT_UPDATE_TFTP \
	FACT_UPDATE_FTP \
	GET_ACTIVE_SOFTWARE \
	LOAD_SW_IMAGES \
	BOOT_APRISALTE

/* Now for the remaining common defines */
#include <configs/ti_armv7_common.h>

#define CONFIG_SYS_USB_FAT_BOOT_PARTITION 1

#endif /* __CONFIG_AM642_APRISALTE_H */
